#!/usr/bin/env bash
composer require drupal/$1 &&
drush en $1 &&
composer update &&
drush cr
