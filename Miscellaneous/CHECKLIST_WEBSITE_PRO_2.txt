

- gitlab avec site web de dev
- securité : module audit sec drupal, antibot, audit conf nginx

- régler les droits d'accès sur le fichier du site e.g. settings ne doit pas être accessible en écriture.
A contrario; le dossier default doit pouvoir recevoir des dossiers.
l'url du site doit être filtrée.


le moteur de templates twig s'occupe a priori des problèmes de XSS
injection sql ? 

- l'env de production doit être mis en place et l'env de dev invalidé.

envisager nettoyage des fichiers inutiles : readme.txt et autre pour anonimiser le site.


- https certificate

- ACCESSUBILITE
- vérif formatage html

- perf : cache agreg drupal, reverse proxy nginx, varnish, modules inutiles
- systeme de recherche : drupal, solr, elasticsearch
- system mail
- system auth : CAS, LDAP
- SEO : xmlsitemap pathauto...
- doc : user, modules, admin (maj etc)

