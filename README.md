# drupal_install

Automatic drupal 8+ "bare metal" or "virtual" install and configuration of drupal itself, of nginx and postgresql


1 - Customize your installation

To begin with; browse the files in the scripts folder and customize them. 

Particularly for a fresh ubuntu, you need to configure global.sh, ubuntu.sh, postgres.sh, nginx.sh, composer.sh.
drush.sh and console.sh are usefull for dev and admin purposes.

so you will need sudo rights for that.

Then, the script drupal.sh is pretty much the only script to configure with drupal_install.sh.
In the latter, you should comment in the call to on-shot scripts like ubuntu.sh

In drupal.sh you can configure the name of your project e.g. mydrupalproj so that the site will be accessible through url: mydrupalproj:9999
You can change the port number in nginx.sh. 
You can configure the language too to french : for that the folder "translations" is copied in the drupal directory.
It contains the translation *.po file downloaded from https://localize.drupal.org/download.
Hence you can translate drupal from english to any language listed in the previous site: 
search for "fr" in drupal_install.sh and drupal.sh replace it accordingly.


It might be necessary to update the *.po file from time to time as "composer update" might not be able to do the job ?

2 - Proceed to installation ./drupal_install

Launch the script drupal_install.sh and type your sudo password each time it is requested (~3 times).
At the end of the process, firefox should open your website and you are free to use your new drupal website.


NB 1: This install system work with ubuntu 20.04 but it should be easy to use on any other distribution like debian, fedora, etc. 
provided you adapt the commands : apt for debian, dnf for fedora, etc

NB 2: It is likely that the first time the script does not work properly but as it uses only bash scripts
with a basic linux admin knowledge, you should be able to troubleshoot the problem mastering all the process from scratch.
It suffices to read the error messages outputted in your terminal and re-run the script untill firefox shows your new drupal website.
You might have to do this from scratch by deleting the drupal project directory.
It is not the case if you add complexity layers using other tools altough you can use this method to build docker images or other
tools/methods you can find on github.com, gitlab.com and so on.

NB 3: The time you will spend debugging/customizing this install system will worth it as after that you will be able
to work on your PC with any number of drupal sites installed quickly. Translation and fix permissions may take some time though.
