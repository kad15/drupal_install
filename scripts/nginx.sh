#!/usr/bin/env bash

############ NGINX VARS TO CUSTOMIZE AND WEB SERVER CONFIGURATION  ###################################


# WEBSERVER NGINX
NGINX_PATH=/etc/nginx
WEBSERVER_GROUP='www-data'

WEBSERVER_HOST='localhost'
if [ -z $TLD ]; then
  TLD="com" # default value
fi
# custom value
TLD="fr" # the top level domain is mandatory in the context of 
# this drupal install system 
# It is a choice made to not complexify the trusted_host_patterns settings

DOMAIN="$DRUPAL_DIR.$TLD" # domain or second level domain
SITE_MAIL="admin@$DOMAIN"
SUB_DOMAIN=www.$DOMAIN  # subdomain or third level domain
			# configuration of dns (or static dns file  /etc/hosts if local dev) 
                        # is required here to map localhost and server name to the right IP e.g. 127.0.0.1 for local dev
WEBSERVER_PORT='9999' # configured in /etc/nginx/site-available with sim link to /etc/nginx/site-enable 
# This way the site will be accessible via e.g. the url http://www.training.edu:9999  
# and the index.php in $DRUPAL_ROOT will be processed by php called by the web server
# php will return an html page wich will be sent to the browser for it to display. 

cd $INSTALL_SCRIPT_PATH/$SCRIPTS  # just to be sure we are in the directory which contains the install script

# $(echo $DRUPAL_ROOT | sed 's/\//\\\//g') is used to escape the slash special char for sed 
# it changes the string /home/desr_web/www/training/web in \/home\/desr_web\/www\/training\/web
# and avoid a sed error
DRUPAL_ROOT_SED=$(echo $DRUPAL_ROOT | sed 's/\//\\\//g')
sed "s/SUB_DOMAIN/$SUB_DOMAIN/g;s/DOMAIN/$DOMAIN/g;s/PORT/$WEBSERVER_PORT/g;s/DRUPAL_ROOT/$DRUPAL_ROOT_SED/g" nginx_domain_template > $DOMAIN  
# the above creates a custom file with the same name as the domain of the website $DOMAIN 
# for nginx configuration by replacing URL, ... tokens in nginx_domain_template file 

# $DOMAIN nginx conf file must be copied to /etc/nginx/site-available
# a sim link in site-enable is then necessary
AVAILABLE=$NGINX_PATH/sites-available
ENABLED=$NGINX_PATH/sites-enabled
sudo mv $DOMAIN $AVAILABLE
sudo chown root: $AVAILABLE/$DOMAIN
sudo ln -sf $AVAILABLE/$DOMAIN $ENABLED

# nginx is configured as www-data user in /etc/nginx/nginx.conf
# we copy /etc/nginx/nginx.conf in /etc/nginx/nginx.conf_ori if the latter does not exist (-n option of cp)
sudo cp -n $NGINX_PATH/nginx.conf $NGINX_PATH/nginx.conf_ori
# we copy nginx.conf, which is in the INSTALL_SCRIPT_PATH=$PWD folder, to /etc/nginx folder
sudo cp -f nginx.conf $NGINX_PATH
sudo chown root: $NGINX_PATH/nginx.conf
sudo systemctl restart nginx
