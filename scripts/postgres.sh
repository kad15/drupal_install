#!/usr/bin/env bash

############ SGBD VARS TO CUSTOMIZE AND DATABASE SERVER CONFIGURATION ###################################

#DB_ROOT_NAME=root # not used with postgresql SGBD (may be useful if mysql or mariadb SGBD are used
#DB_ROOT_PASS=password # not used with postgresql SGBD : database root password (i.e. superadmin of the SGBD with all permissions)  

# DATABASE
DB_NAME=db_$DRUPAL_DIR # data base name which will be used by the web site. drupal creates about 71 tables in it during install process
DB_USER='postgres'
DB_USER_PASS='postgres'
DB_HOST='localhost'
DB_PORT='5432'
DB_URL=pgsql://$DB_USER:$DB_USER_PASS@$DB_HOST:$DB_PORT/$DB_NAME

#DB_URL=pgsql://postgres:postgres@$localhost:5432/db_loove

sudo -iu postgres psql -c "CREATE DATABASE $DB_NAME;" &&
sudo -iu $DB_USER psql -c "GRANT ALL PRIVILEGES ON DATABASE $DB_NAME TO $DB_USER;"
# create extension pg_trgm allegedly used by drupal 9 
sudo -iu postgres psql -d $DB_NAME -c "create extension pg_trgm;"

