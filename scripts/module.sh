#!/usr/bin/env bash

# this script install/enable or disable/remove a module
ACTION=$1  # inst|rem, inst for install, rem for remove 
MODULE=$2 # module name with optional version constraint separated by a colon ":"
# MO = module name without version constraint e.g. gin_toolbar:^3@alpha will output gin_toolbar
MO=`echo $MODULE | cut -d':' -f1` 
if [[ $ACTION = "inst" ]]; then
  composer req drupal/$MODULE &&
  drush \e\n $MO -y
elif [[ $ACTION = "rem" ]]; then
  drush pmu $MO && # disable the module
  composer rem drupal/$MO # remove the module from composer.json and from the disk
else
  echo "this script install/enable or disable/remove a module"
  echo "usage: `basename $0` inst|rem module_name" 
  exit 1
fi


