#!/usr/bin/env bash

# INSTALLATION OF composer on UBUNTU 20.04 LTS
# This install is global: the binary "composer" is installed in /usr/local/bin
# But the composer "home" directory is in `composer config --global home` 
cd /home/$LINUX_USER
sudo apt update
sudo apt install wget curl php-cli php-zip unzip


# download the php installer script
wget -O composer-setup.php https://getcomposer.org/installer

# verification if the installer has not been modified for security
#HASH=`curl -sS https://composer.github.io/installer.sig`
#echo $HASH
#php -r "if (hash_file('SHA384', 'composer-setup.php') === '$HASH') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
# if the message "Installer verified" is displayed we can install composer

# install composer
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer

rm composer-setup.php

# write the path for the binaries installed globally via composer
# to be directly usable from the command line: cf. drush and drupal console.
COMP=`composer config --global home`
echo "export PATH=$COMP/vendor/bin:\$PATH" >> ~/.bashrc
source ~/.bashrc

# ********************* GENERAL INFO ***********************************************
# Update global composer.json file and dependencies in the folder given
# by the command: composer config --global home
# composer global update

# update composer
# sudo composer self-update

# composer require --dev drupal/core-dev # for PHPUnit suite core test
# "composer update" command uses the json file to update lock file and php programs
# "composer install" command uses the lock file only to install the php dependencies with the exact versions given in the lock file
# "composer require namespace/pack_name" command add lines to the json file; update the lock file and download the package pack_name
# with the namespace = most of the time to "vendor" name. vendor can be the name of an other programmer who maintain the package or anything
# which is not drupal or our own php code.   

# INSTALLATION DE COMPOSER cf. google
# composer download drupal and its dependencies. drush is then used to install drupal 
# LOCAL DRUSH INSTALL in the vendor folder : DRUSH stands for drupal sh, a shell for drupal
# composer require drush/drush
# installation of drush globally
#composer global require drush/drush

# install of drupal console globally
# drupal console is usefull to generate code during drupal dev processus 
# sudo apt -y install curl
#curl https://drupalconsole.com/installer -L -o drupal.phar
#sudo mv drupal.phar /usr/local/bin/drupal
#sudo chmod +x /usr/local/bin/drupal # drupal console is now usable anywhere through the command "drupal"
# drupal console had to be installed with composer in the drupal directory which contains web, vendor folders and composer.json + lock files
# cd $DRUPAL_PATH 
# composer require drupal/console:^1 --prefer-dist --optimize-autoloader --sort-packages --no-update
# ^1 means version 1 and above will be updated via composer update until version 2 is reached ; see semantic version to avoid breaking the app
# composer update


# drupal console local install in drupal site folder using composercompser 
# https://drupalconsole.com/docs/en/getting/composer
# composer update drupal/console --with-dependencies
