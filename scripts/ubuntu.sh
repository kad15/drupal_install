#!/usr/bin/env bash

# CONFIG FOR UBUNTU 20.04 LTS
# nginx, postgres and php-fpm with the pg_trgm extension must be installed ; 
# php will not work with nginx unless php-fpm fast-cgi is installed
# Drupal 9 requires at least PHP 7.3. PHP 7.4 is also supported 
# but not required. PHP 8 is supported from Drupal 9.1.0
# If using Drupal 9 with PostgreSQL, version 10 is required with the pg_trgm extension.
# If you are running Drupal 9 on Nginx, at least version 0.7.x is required.


#Drupal 9 system requirements
#    PHP >=7.3 with php-fpm 
#    nginx > 0.7.x 
#    PostgreSQL >=10 with the pg_trgm extension
#    MySQL or Percona, version >=5.7.8
#    MariaDB >=10.3.7


sudo apt update 
sudo apt upgrade -y
sudo apt install software-properties-common -y
# PHP INSTALL 
PHP_VER=8.0
sudo apt install php$PHP_VER -y sudo apt install -y php$PHP_VER-cli php$PHP_VER-fpm php$PHP_VER-opcache php$PHP_VER-mbstring php$PHP_VER-xml php$PHP_VER-gd php$PHP_VER-curl php$PHP_VER-soap php$PHP_VER-mysql

# xdebug install for dev environment
sudo apt install php-xdebug  

# if necessary uncomment those lines
# enable will start php-fpm service at boot time
sudo systemctl enable php8.0-fpm 
sudo systemctl start php8.0-fpm 

#Switch between multiple PHP version
#sudo update-alternatives --config php
# passage en php 7.4 ; php 8 +/- incompatible avec drupal 8
#printf "2\n" | sudo update-alternatives --config php

# NGINX INSTALL
sudo apt-get install nginx -y

# if necessary encomment those lines 
sudo systemctl restart nginx
sudo systemctl enable nginx
sudo systemctl status nginx

# firewall config if necessary
sudo ufw allow 'Nginx Full'
sudo ufw reload
# some modifications of php.ini might be necessary in terms of memory and file size downloadable

# POSTGRES INSTALL
#Drupal 9 requires PostgreSQL 10.0 or higher with the pg_trgm extension
# install the Postgres package along with a -contrib 
#package that adds some additional utilities and functionality:
sudo apt install postgresql postgresql-contrib

# if necessary encomment those lines
sudo systemctl restart postgresql
sudo systemctl enable postgresql
sudo systemctl status postgresql


# composer global install
source composer.sh

# drush global install
source drush.sh

# console global install : although there are some pb using drupal console globally installed # drupal site:status fails
# locally installed console works: vendor/drupal/console/bin/drupal site:status 
# source console.sh

