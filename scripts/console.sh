#!/usr/bin/env bash

# drupal console install  FOR UBUNTU 20.04 LTS
# This install is global but not system wide: the binary "drupal" is installed 
# in ~/.config/composer/vendor/bin 
# Warning : drupal console installed globally does not seem to work properly
# It might be compulsory to use a per-site drupal console install.

# install of drupal console on UBUNTU 20.04 LTS
# The binary for drupal console is called "drupal"
composer global require drupal/console:@stable \
--prefer-dist \
--optimize-autoloader \
--sort-packages \
--no-update



# if composer has not bin installed yet and .bashrc not updated
#COMP=`composer config --global home`
#echo "export PATH=$COMP/vendor/bin:\$PATH" >> ~/.bashrc


# nb : The command "drupal list" display all options available

# composer global require drupal/console:@stable


