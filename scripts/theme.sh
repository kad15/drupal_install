#!/usr/bin/env bash

# this script download/install/enable or disable/remove a default theme. i.e the default theme is the active theme 

ACTION=$1  # inst|rem, inst for install, rem for remove 
THEME=$2
# THEME without version constraint gin:^3@alpha will output gin
TH=`echo $THEME | cut -d':' -f1` 
if [[ $3 = "admin" ]]; then
  THEME_TYPE=$3
else
  THEME_TYPE='default'
fi

if [[ $ACTION = "inst" ]]; then
  composer require drupal/$THEME &&
  drush \t\h\e\n $TH &&
  drush config:set system.theme $THEME_TYPE $TH -y
elif [[ $ACTION = "rem" ]]; then
echo "drush config:set system.theme $THEME_TYPE bartik -y"
  drush config:set system.theme $THEME_TYPE bartik -y && 
  drush thun $TH
  composer remove drupal/$TH
else
  echo "this script download/install/enable or disable/remove a default theme"
  echo "usage: "`basename $0`" inst|rem theme_name" 
  echo
  echo "Example : `basename $0` inst my_theme admin will install and activate my_theme as an admin theme."
  echo "Without the admin parameter, my_theme will become the active theme."  
  exit 1
fi
