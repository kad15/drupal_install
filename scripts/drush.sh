#!/usr/bin/env bash

# drush INSTALLATION AND CONFIGURATION FOR UBUNTU 20.04 LTS
# This install is global but not system wide: the binary "drush" is installed 
# in ~/.config/composer/vendor/bin 
sudo apt-get update 
sudo apt install php-xml php-mbstring

# the global parameter tels composer to use the composer.json file
# in ~/.config/composer. this directory is given by the command
# composer config --global home
sudo composer global require drush/drush

# To update Drush to the newest version, type the following command:
# composer global update drush/drush


# if composer has not bin installed yet and .bashrc not updated
#COMP=`composer config --global home`
#echo "export PATH=$COMP/vendor/bin:\$PATH" >> ~/.bashrc
