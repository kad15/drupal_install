#!/usr/bin/env bash

############ DRUPAL VARS TO CUSTOMIZE ################################
# The web site will be accessible via http://$DRUPAL_DIR:9999 
DRUPAL_DIR='kad'  # top level domain see below

# website default language. translation must have downloaded first
LOCALE=fr
# LOCALE=en 

# nb : when drupal is installed using composer the directories (scafolding) are different from that of when
# one download the drupal tar.gz file or via the gitlab drupal project repository ; 
# The difference is the web folder. with composer the public folder visible from outside 
#is $WWW_PATH/$DRUPAL_DIR/web
# e.g. if 127.0.0.1 enm-toulouse.tld is added to /etc/hosts and if nginx is configured properly
# with e.g. port 9999 and the same domain enm-toulouse.tld in the nginx enm-toulouse conf file
# and the correct root directory i.e. $WWW_PATH/$DRUPAL_DIR/web. This way the url http://enmtoulouse.tld:9999
# is maped withe the root directory of the website. 
# nb : it is only a convention to choose the name enm-toulouse both for the drupal directory
# and for the nginx conf file in /etc/nginx/sites-available/ . Moreover, the domain name
# is formed by appending the top level domain e.g. .tld .edu .fr or whatever.
# The database name is formed by prefixing the drupal directory name with db_ : db_enm-toulouse
# nb : the hyphen(-) in the name of the dabase may cause problem with database queries ; had to be escaped
# such as db_enm\-toulouse to create the db. so the website is named enmtoulouse which is the title 
# of the website too. so the variable DRUPAL_DIR is used to name 4 different things : 
# 1 - the drupal directory
# 2 - the domain with ".tld"
# 3 - the database with "db_"
# 4 - the default title of the site which will be displayed on the home page
# nb : the domain is in fact the second level domain "enmtoulouse.tld" with the trailling top level tld.
# see nginx and postgres configuration below   
DRUPAL_PATH=$WWW_PATH/$DRUPAL_DIR #  it's the app root in /home/$LINUX_USER/www/$DRUPAL_DIR
DRUPAL_ROOT=$DRUPAL_PATH/web   # it's the public folder (or web root) intented to be seen from outside; to put in web server configuration cf. /etc/nginx

# NB : this web folder exist only when installing 

SITE_NAME=$DRUPAL_DIR
#SITE_MAIL="admin@$DOMAIN" # var DOMAIN is not yet defined
ACCOUNT_NAME=admin # web site admin name ; it will be the user number 1 which have all the rights on the drupal web site
ACCOUNT_PASS=admin # credentials for drupal's website administrator 

# config od the folder where drupal configuaration yml files will be written
CONFIG_SYNC_DIRECTORY="$DRUPAL_PATH/config/global"

 
