#! /usr/bin/env bash

# usage : go to the scripts folder and read and customize *.sh files as needed
# mainly global.sh, drupal.sh and nginx.sh are to be modified

# set -e # exit on error

SCRIPTS="scripts"



# install ubuntu packages on a fresh ubuntu 20.04 install
# uncomment the line below if necessary
# source $SCRIPTS/ubuntu.sh

# global env variables to customize
source $SCRIPTS/global.sh

# drupal env variables to customize
source $SCRIPTS/drupal.sh

# postgres env variables to customize
source $SCRIPTS/postgres.sh

# nginx env variables to customize
source $SCRIPTS/nginx.sh


# gives execute permission to all the scripts
chmod u+x -R $INSTALL_SCRIPT_PATH/$SCRIPTS


# WWW_PATH is a folder like  ...../www where we put all our websites, 
# DRUPAL_PATH is the drupal directory:  ..../www/$DRUPAL_DIR, 
# DRUPAL_ROOT is the drupal public directory:  ..../www/$DRUPAL_DIR/web
mkdir -p $WWW_PATH && cd $WWW_PATH


# The drupal/recommended-project creates a new Drupal site with a "relocated document root"
# named "web" vs the different directory structure of drupal on gitlab https://git.drupalcode.org/project/drupal
# This means that the files "index.php" and the "core" directory and so on are placed 
# inside a subfolder named "web" rather than being placed next to "composer.json" and 
# the "vendor" directory at the project root. This layout is #recommended because 
# it allows you to configure your webserver to only provide access to files inside 
# the "web" directory. Keeping the vendor directory outside #of the web server's 
# document root is better for security.
# see https://www.drupal.org/docs/develop/using-composer/starting-a-site-using-drupal-composer-project-templates  for more information

if [ ! -d "$DRUPAL_DIR" ]; then
  # if $DRUPAL_DIR doesn't exist composer will download drupal and all its dependencies 
 composer create-project drupal/recommended-project $DRUPAL_DIR
#composer create-project --stability dev --no-interaction front/university-project $DRUPAL_DIR # does not work this distro needs php 7 not 8 but composer needs php 8 !
fi

# to install dev dependencies  
# composer require drupal/core-dev

sudo chmod 775 $DRUPAL_PATH
cd $DRUPAL_PATH

# drush local install
composer req drush/drush

# drupal console local install 
# post install test : vendor/drupal/console/bin/drupal site:status
composer require drupal/console:^1.0 \
--prefer-dist \
--optimize-autoloader \
--sort-packages \
--no-update

composer update

mkdir -p $CONFIG_SYNC_DIRECTORY # custom "sync" directory that receives yml config files during an "export" of the active config which is in the database
# conversely an "import" means loading config yml files into the database. config/global is arbitrary chosen ; any name would do the trick.
# it is place out of the public web directory as it is a good practice not to. Cf. file /sites/default/settings.php. it is in it that the SYNC
# directory is configured in order for drupal to read or write the config. drupal must write in the sync directory, hence the 777 permissions below 

chmod -R 777 $CONFIG_SYNC_DIRECTORY



# translation folder comprises drupal-9.2.fr.po translation file in french of drupal core
if [[ "$LOCALE" = "fr" ]]
then
  cp -arf $INSTALL_SCRIPT_PATH/translations $DRUPAL_ROOT/sites/default/files/
fi


# use a recent version of “drush site-install” using its new “--config-dir=config/sync” option. 
# This command will install your profile, then patch the site UUID** and then perform a config-import from the specified folder.
# --config-dir=$CONFIG_SYNC_DIRECTORY   here this folder is named config/global and put outside the "web" public directory of drupal 
# ** UUID stands for Universally Unique IDentifier
# From time to time it comes in quite handy to import configuration 
# from other websites into yours. Your site uuid is unique though, with the following snippet you get yours.
# drush config-get "system.site" uuid
# To set the uuid in drush:
# drush cset system.site uuid 9b13adbf-0da5-4937-a731-xxxxxxxxxxx -y

PROFILE=standard
drush -y site-install $PROFILE \
--locale="$LOCALE" \
--db-url="$DB_URL" \
--account-name="$ACCOUNT_NAME" --account-pass="$ACCOUNT_PASS" --account-mail="$SITE_MAIL" \
--site-name="$SITE_NAME" --site-mail="$SITE_MAIL" 

# install and configuration of drupal using standard install profile.
# this install phase is performed via commmand line which is better than using 
# the drupal interface via the scrit install.php.
# standard is a profile with some configs
# other profiles : 
# minimal : no preconfig
# demonstration : install a food recipes website called "umami" with preconfig. content
# 
# --account-mail : uid1 email. Defaults to admin@example.com
# --existing-config="$DRUPAL_DIR/config/global"  in case there is config yaml files to load
# --site-mail : From: for system mailings. Defaults to admin@example.com  use the same domain as the site to avoid email rejected as spams
# !!!!! --config-dir=CONFIG-DIR. Deprecated - only use with Drupal <= 8.5- !!!!
# A path pointing to a full set of configuration which should be installed during installation.
# --existing-config. Configuration from sync directory should be imported during installation. Use with Drupal 8.6+.


# remove config folder created by drupal to use custom config folder $DRUPAL_PATH/config/global 
rm -rf $DRUPAL_ROOT/sites/default/files/config*

# git
cp -af $INSTALL_SCRIPT_PATH/tpl.gitignore $DRUPAL_PATH/.gitignore
 



sudo chmod -R 775 $DRUPAL_ROOT/sites &&
sudo chown -R $LINUX_USER:$WEBSERVER_GROUP $DRUPAL_ROOT/sites

cd $DRUPAL_ROOT/sites/default

# renaming original file
# the below local file is used to override settings for dev purposes
FIC="settings.local.php"
if [[ -f "$FIC" ]]; then
     cp $FIC $FIC.ori
fi

# renaming original file
FIC="settings.php"
if [[ -f "$FIC" ]]; then
    mv $FIC $FIC.ori
fi


# drupal mode configuration : dev or prod 
# copy of settings template files directory
# If MODE=dev the settings/sites folder is copied in drupal public directory 
# namely the "web" directory in drupal directory. the same applies for MODE="prod"
# or any other configuration we may think of, providing we create a folder 
# .../settings/other_config/sites 
sudo cp -arf $INSTALL_SCRIPT_PATH/settings/$MODE/* $DRUPAL_ROOT

# if TLD is not set the domain string with the point escaped 
if [ -z $TLD ]; then
  your code
fi



# security filter for urls
# NB : if those settings are incorrect you will get 
# the message "The provided host name is not valid for this server" from your browser
# NB : we have to escape de the dot symbol as regexp interpret it as "any char" !!!
sudo echo " " >> $FIC
sudo echo '# SECURITY REGEXP URL FILTER '  >> $FIC 
sudo echo "\$settings['trusted_host_patterns'] = [" >> $FIC
sudo echo "'^$DRUPAL_DIR\.$TLD$'," >> $FIC
sudo echo "'^.+\.$DRUPAL_DIR\.$TLD$'," >> $FIC
sudo echo "];" >> $FIC



# config settings file with sensitive data : dg credentials, ... 
sudo echo " " >> $FIC
sudo echo '# DATABASE CONFIGURATION. BEWARE ! SENSITIVE DATA'  >> $FIC 
sudo echo "\$databases['default']['default'] = [" >> $FIC
sudo echo "'database' => '$DB_NAME'," >> $FIC
sudo echo "'username' => '$DB_USER'," >> $FIC
sudo echo "'password' => '$DB_USER_PASS'," >> $FIC
sudo echo "'prefix' => ''," >> $FIC
sudo echo "'host' => 'localhost'," >> $FIC
sudo echo "'port' => '5432'," >> $FIC
sudo echo "'namespace' => 'Drupal\\Core\\Database\\Driver\\pgsql'," >> $FIC
sudo echo "'driver' => 'pgsql'," >> $FIC
sudo echo "];" >> $FIC

sudo echo " " >> $FIC
echo '#setting of the "sync" directory to export drupal site config yaml files from the database' >> $FIC
# reminder : CONFIG_SYNC_DIRECTORY="$DRUPAL_PATH/config/global"
sudo echo "\$settings['config_sync_directory'] = '$CONFIG_SYNC_DIRECTORY';" >> $FIC


#########################  FIX PERMISSIONS #########################################

bash $INSTALL_SCRIPT_PATH/$SCRIPTS/fix_permissions.sh $DRUPAL_ROOT $LINUX_USER $WEBSERVER_GROUP

#####################################################################################


echo
echo
echo "Updating /etc/hosts file to access to the site via url $DOMAIN:$WEBSERVER_PORT"
# Append line in hosts file : 127.0.1.1 $DOMAIN
cat /etc/hosts | grep $DOMAIN
GREP_RETURN_CODE=$?
# if grep command return a non zero code then $DOMAIN does not exist so it is added in hosts file
if [[ $GREP_RETURN_CODE -ne 0 ]]
then
   sudo bash $INSTALL_SCRIPT_PATH/$SCRIPTS/maj_hosts.sh "127.0.1.1 $DOMAIN $SUB_DOMAIN"
   echo "/etc/hosts updated."
else
   echo "/etc/hosts already configured.Nothing to do !"
fi
cat /etc/hosts | grep $DOMAIN
echo 
echo
echo
echo "***********************************************************************************"
echo "END OF INSTALLATION : TO ACCESS TO DRUPAL USE $DOMAIN:$WEBSERVER_PORT IN A BROWSER" 
echo "***********************************************************************************"

# rebuild all caches and copy a custom composer.json file
# JSON=composer.json
# cd $DRUPAL_PATH
# mv $JSON $JSON.ori
# cp $INSTALL_SCRIPT_PATH/$JSON .

# composer updates all modules and themes under
# the version constraints in the json file and download
# all the modules and dependencies. the composer.lock file is updated too. 
# composer update

drush cr

sudo systemctl restart nginx &&
firefox $DOMAIN:$WEBSERVER_PORT &

# post install and configurations. To uncomment after testing

#bash $INSTALL_SCRIPT_PATH/$SCRIPTS/theme inst gin:^3@alpha admin
#bash $INSTALL_SCRIPT_PATH/$SCRIPTS/module inst gin_login:^1@rc
#bash $INSTALL_SCRIPT_PATH/$SCRIPTS/module inst gin_toolbar:^1@beta
#bash $INSTALL_SCRIPT_PATH/$SCRIPTS/module inst admin_toolbar:^2  # !! version 3 leads to errors 502 gateway or other bugs
#bash $INSTALL_SCRIPT_PATH/$SCRIPTS/module inst admin_toolbar_tools:^2


# TODO : copier le composer.json pour installer les modules
# et les thèmes en particulier gin gin_toolbar gin_login admin_toolbar:^2
# les activer faire un drush cr
# modules and themes are only downloaded but not activated in drupal using composer
# is it possible to do post install actions like changing the default and admin themes.
# solution using a custom profile ?




